from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'boards', views.BoardViewSet)
router.register(r'threads', views.ThreadViewSet)
router.register(r'posts', views.PostViewSet)

urlpatterns = [
    path('', views.index, name='index'),
    path('', include(router.urls)),
    path('<int:board_id>/', views.view_board, name='view_board'),
    path('create/', views.create_board, name='create_board'),
    path('<int:board_id>/<int:thread_id>/', views.view_thread, name="view_thread"),
    path('<int:board_id>/<int:thread_id>/reply', views.reply_to_thread, name="reply_to_thread"),
    path('<int:board_id>/<int:thread_id>/lock', views.lock_thread, name="lock_thread"),
    path('users/<int:user_id>/', views.view_user, name="view_user"),
    path('<int:board_id>/create/', views.create_thread, name="create_thread")
]
