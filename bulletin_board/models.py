from django.contrib.auth.models import User
from django.db import models


class Board(models.Model):
    name = models.CharField(max_length=100)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    created_on = models.DateTimeField('date created')
    homepage_position = models.PositiveIntegerField()

    def __str__(self):
        return self.name

    def latest_thread_post(self):
        latest_thread = self.threads.order_by('-last_updated')
        if latest_thread:
            return latest_thread[0].id

    def post_count(self):
        posts = 0
        threads = self.threads.all()
        for thread in threads:
            posts += thread.post_set.count()
        return posts


class Thread(models.Model):
    title = models.CharField(max_length=100)
    posted_by = models.ForeignKey(User, on_delete=models.CASCADE)
    posted_on = models.DateTimeField('posted on')
    is_locked = models.BooleanField(default=False)
    board = models.ForeignKey(Board, related_name='threads', on_delete=models.CASCADE)
    last_updated = models.DateTimeField('last updated', null=True)

    class Meta:
        ordering = ['-posted_on']
        permissions = (("can_change_lock_status", "Can change lock status"),)

    def __str__(self):
        return self.title

    def latest_post(self):
        latest_post = self.posts.order_by('-posted_on')
        if latest_post:
            return latest_post[0]


class Post(models.Model):
    message = models.TextField()
    posted_on = models.DateTimeField('posted on')
    posted_by = models.ForeignKey(User, on_delete=models.CASCADE)
    thread = models.ForeignKey(Thread, related_name="posts", on_delete=models.CASCADE)

    class Meta:
        ordering = ['-posted_on']

    def __str__(self):
        return self.message
