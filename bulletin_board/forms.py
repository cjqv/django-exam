from django.forms import ModelForm

from .models import Board, Thread, Post


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['message']


class BoardForm(ModelForm):
    class Meta:
        model = Board
        fields = ['name', 'homepage_position']


class ThreadForm(ModelForm):
    class Meta:
        model = Thread
        fields = ['title']
