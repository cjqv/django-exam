from rest_framework import serializers
from .models import Board, Thread, Post


class PostSerializer(serializers.ModelSerializer):
    posted_by = serializers.ReadOnlyField(source="posted_by.username")

    class Meta:
        model = Post
        fields = ['message', 'posted_by', 'posted_on']


class ThreadSerializer(serializers.ModelSerializer):
    posted_by = serializers.ReadOnlyField(source="posted_by.username")
    posts = PostSerializer(many=True, read_only=True)

    class Meta:
        model = Thread
        fields = ['title', 'posted_by', 'posted_on', 'is_locked', 'board', 'last_updated', 'posts']


class BoardSerializer(serializers.ModelSerializer):
    creator = serializers.ReadOnlyField(source="creator.username")
    latest_thread = serializers.IntegerField(source="latest_thread_post")

    class Meta:
        model = Board
        fields = ['name', 'creator', 'created_on', 'latest_thread']
