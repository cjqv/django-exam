from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from rest_framework import viewsets

from .forms import PostForm, BoardForm, ThreadForm
from .models import Board, Thread, Post

from .serializers import BoardSerializer, ThreadSerializer, PostSerializer


def index(request):
    board_list = Board.objects.order_by('homepage_position')
    current_user = request.user
    if current_user.has_perm('can_add_board'):
        can_add_board = True
    else:
        can_add_board = None
    return render(request, "bulletin_board/index.html",
                  {
                      "board_list": board_list,
                      "can_add_board": can_add_board,
                  })


def create_board(request):
    if request.method == "POST":
        form = BoardForm(request.POST)
        if form.is_valid():
            b = Board(name=form.cleaned_data["name"], creator=request.user,
                      created_on=timezone.now(), homepage_position=form.cleaned_data["homepage_position"])
            b.save()
            return HttpResponseRedirect('/')
    else:
        form = BoardForm()

    return render(request, "bulletin_board/create.html", {"form": form})


def view_board(request, board_id):
    board = get_object_or_404(Board, pk=board_id)
    threads = board.thread_set.all()
    return render(request, "bulletin_board/details.html",
                  {
                      "board": board,
                      "threads": threads
                  })


def view_thread(request, board_id, thread_id):
    thread = get_object_or_404(Thread, pk=thread_id)
    posts = thread.post_set.all()
    current_user = request.user
    if current_user.is_authenticated and not thread.is_locked:
        post_form = PostForm()
    else:
        post_form = None

    if current_user.has_perm("can_change_lock_status"):
        change_lock_status = True
    else:
        change_lock_status = False

    return render(request, "thread/details.html",
                  {
                      "board_id": board_id,
                      "thread": thread,
                      "posts": posts,
                      "post_form": post_form,
                      "change_lock_status": change_lock_status
                  })


def create_thread(request, board_id):
    if request.method == "POST":
        form = ThreadForm(request.POST)
        if form.is_valid():
            b = Board.objects.get(pk=board_id)
            t = Thread(title=form.cleaned_data["title"], posted_by=request.user, posted_on=timezone.now(), board=b)
            t.save()
            return HttpResponseRedirect('/%s/' % board_id)
    else:
        form = ThreadForm()

    return render(request, "thread/create.html",
                  {
                      "form": form,
                      "board_id": board_id
                  })


def reply_to_thread(response, board_id, thread_id):
    if response.method == "POST":
        thread = get_object_or_404(Thread, pk=thread_id)
        form = PostForm(response.POST)
        if form.is_valid():
            p = Post(message=form.cleaned_data["message"], thread=thread,
                     posted_on=timezone.now(), posted_by=response.user)
            thread = p.thread
            thread.last_updated = timezone.now()
            thread.save()
            p.save()
            return HttpResponseRedirect('/%s/%s/' % (board_id, thread_id))


def view_user(request, user_id):
    user = User.objects.get(pk=user_id)
    posts = user.post_set.all()
    return render(request, "users/user.html",
                  {
                      "user": user,
                      "posts": posts
                  })


def lock_thread(request, board_id, thread_id):
    thread = get_object_or_404(Thread, pk=thread_id)
    thread.is_locked = True if not thread.is_locked else False
    thread.save()
    return HttpResponseRedirect('/%s/%s/' % (board_id, thread_id))


class BoardViewSet(viewsets.ModelViewSet):
    queryset = Board.objects.all()
    serializer_class = BoardSerializer


class ThreadViewSet(viewsets.ModelViewSet):
    queryset = Thread.objects.all()
    serializer_class = ThreadSerializer


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
